<?php

$integers1 = [0, 2, 1, 3, 8, 5, 4,];
$integers2 = [1, 2, 7, 4, 10,];

function getCommonIntegers(array $integers1, array $integers2): array
{
    $commonIntegers = [];

    foreach ($integers1 as $integer1) {
        foreach ($integers2 as $integer2) {
            if ($integer1 === $integer2) {
                $commonIntegers[] = $integer1;
            }
        }
    }

    return $commonIntegers;
}

$commonIntegers = getCommonIntegers($integers1, $integers2);

echo implode(", ", $commonIntegers)."\n";
