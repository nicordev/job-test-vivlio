<?php

function myInArray(int $needle, array $haystack): bool
{
    foreach ($haystack as $straw) {
        if ($straw === $needle) {
            return true;
        }
    }

    return false;
}

function getCommonIntegers(array $integers1, array $integers2): array
{
    $commonIntegers = [];

    foreach ($integers1 as $integer1) {
        if (myInArray($integer1, $commonIntegers)) {
            continue;
        }

        foreach ($integers2 as $integer2) {
            if ($integer1 === $integer2) {
                $commonIntegers[] = $integer1;
                break;
            }
        }
    }

    return $commonIntegers;
}

test('get same elements', function () {
    $results = getCommonIntegers(
        [9,6,3,1,8,23,7657,2],
        [14,4326,675,32,2,5474,1,435,675,3],
    );
    sort($results);
    expect($results)->toBe([1,2,3]);
});

test('get same elements with other values', function () {
    $results = getCommonIntegers(
        [4326, 5474],
        [14,4326,675,32,2,5474,1,435,675,3],
    );
    sort($results);
    expect($results)->toBe([4326,5474]);
});

test('get same elements with duplicates', function () {
    $results = getCommonIntegers(
        [4326, 5474],
        [14,4326,675,32,2,5474,1,435,5474,675,3],
    );
    sort($results);
    expect($results)->toBe([4326,5474]);
});
