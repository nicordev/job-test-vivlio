# symfony-job-interview-test

## Exercice 1

With php installed locally:

```bash
php exercise-1/question-1.php
php exercise-1/question-2.php
```

Or using docker:

```bash
make exercise-1-question-1
make exercise-1-question-2
```

## Exercice 2

## Question 1

> What does this project do? Give details to explain.

The application redirects the user to an URL corresponding to the token that is in the user's request.

When a user browses `/redirect?token=someKnownTokenHere`, the user is redirected to the URL stored in the application's database corresponding to the token.

Some gherkin to make it clearer:

Scenario: the user uses a known token

Given the application's URL is `https://redirect-application.com`
Given a URL `https://my-awesome-website.com?whichUrlIsVery=looooooooooooooong` corresponding to the token `my-awesome-token` stored in the application's database
Given a user of the application
When the user browses `https://redirect-application.com/redirect?token=my-awesome-token`
Then the user is redirected to `https://my-awesome-website.com?whichUrlIsVery=looooooooooooooong`

## Question 2

> Did you see important or strange things?

Overall:
- the `.env` file is empty, so the application can't access the database
- no migration files nor Entity classes to generate the database table

Method `RedirectController::index`:
- lines 12 and 14: SQL injection risks by using directly the query parameter inside SQL queries
- it's not the role of a controller to query the database, so the Single Responsibility Principle of SOLID is not met
- line 12:
    - the method `Connection::fetchAll` does not exist in the `Connection` class
    - accessing the first item of an array that can be empty will throw a PHP error
- the direct use of `$_GET` instead of Symfony `Request` object
- the direct use of the database connection instead of using a Doctrine repository
- the use of `header('Location: ' . $uri['url']);` instead of using Symfony method: `AbstractController::redirect`
- the use of `exit` instead of returning a Symfony `Response` object
- missing return type in the method's signature

File `composer.json`:
- the use an old version of php 7 that is not supported anymore: [php supported versions](https://www.php.net/supported-versions.php)
- the use of an unmaintained Symfony version 3.4

## Question 3

> What important improvements could you propose? Give us 3 improvements if possible.

- [x] add the missing environment variable `DATABASE_URL` in the `.env` file to allow the application to use the database
- [x] add an Entity `Uri` and an EntityRepository `UriRepository` that can be called from the controller `RedirectController` to fetch the data from the database
- [x] make the controller extend `AbstractController` to use `AbstractController::redirectToRoute` like so: `return $this->redirect($url);`
- [x] use a Symfony `Request $request` parameter in the `RedirectController::index` method to retrieve the token using `$token = $request->query->get('token');`
- [x] write PHPUnit tests
- [ ] use a database dedicated tests
- [ ] update to Symfony 5.4 at least
- [ ] use migrations files
- [ ] use doctrine fixtures
- [ ] add some tools like xdebug, PHP CS Fixer, PHPStan, infection

Possible features:
- add a dashboard to manage Uri's
- add a CRUD API to manage Uri's

### Install notes

```bash
make install
```

### How to test

```bash
make test
```

### How to use

Go to https://127.0.0.1/redirect?token=some_token
