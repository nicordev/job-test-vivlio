<?php
namespace App\Controller;

use App\Repository\UriRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RedirectController extends AbstractController
{
    public function index(Request $request, UriRepository $uriRepository)
    {
        $token = $request->query->get('token');

        if (null === $token || '' === $token) {
            throw new BadRequestHttpException('missing or empty token parameter');
        }

        $uri = $uriRepository->findOneByToken($token);

        if (null === $uri) {
            throw new BadRequestHttpException('unknown token');
        }

        $uri->setTimesUsed($uri->getTimesUsed() + 1);

        return $this->redirect($uri->getUrl());
    }
}
