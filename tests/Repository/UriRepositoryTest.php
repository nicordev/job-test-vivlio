<?php

namespace App\Tests\Repository;

use App\Entity\Uri;
use App\Repository\UriRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UriRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testFindOneByToken(): void
    {
        /** @var UriRepository $uriRepository */
        $uriRepository = $this->entityManager
            ->getRepository(Uri::class)
        ;
        $uri = $uriRepository->findOneByToken('helloWorld');

        $this->assertInstanceOf(Uri::class, $uri);
        $this->assertEquals('nicordev.com', $uri->getUrl());
    }
}
