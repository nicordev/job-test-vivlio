<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RedirectControllerTest extends WebTestCase
{
    public function test_index_missing_token_parameter(): void
    {
        $client = static::createClient();
        $client->request('GET', '/redirect');

        $this->assertSame(400, $client->getResponse()->getStatusCode());
    }

    public function test_index_empty_token_parameter(): void
    {
        $client = static::createClient();
        $client->request('GET', '/redirect');

        $this->assertSame(400, $client->getResponse()->getStatusCode());
    }

    public function test_index_unknown_token_parameter(): void
    {
        $client = static::createClient();
        $client->request('GET', '/redirect?token=unknownToken');

        $this->assertSame(400, $client->getResponse()->getStatusCode());
    }

    public function test_index_known_token_parameter(): void
    {
        $client = static::createClient();
        $client->request('GET', '/redirect?token=helloWorld');

        $response = $client->getResponse();
        $this->assertSame(302, $response->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertSame('nicordev.com', $response->getTargetUrl());
    }
}
