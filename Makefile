# Exercise 1

exercise-1-question-1:
	docker run --rm -it --volume=$$PWD/exercise-1:/app --workdir=/app php:7.2-fpm-stretch php question-1.php

exercise-1-question-2:
	docker run --rm -it --volume=$$PWD/exercise-1:/app --workdir=/app php:7.2-fpm-stretch php question-2.php

.PHONY: exercise-1-question-1 exercise-1-question-2

# Exercise 2

USER = $(shell id -u)
EXEC = docker-compose exec --user ${USER}
EXEC_APP = $(EXEC) app
EXEC_PROXY = $(EXEC) proxy
EXEC_DB = $(EXEC) db
EXEC_APP_PHP_BIN_CONSOLE = $(EXEC_APP) app php bin/console

ifdef filter
	FILTER = --filter ${filter}
endif

build:
	docker-compose build

install: build start composer-install database fixtures

database:
	$(EXEC_DB) mysql -uroot -pnotsecret symfony-job-interview -e "DROP TABLE IF EXISTS uri; CREATE TABLE uri (id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, url VARCHAR(1000) NOT NULL, token VARCHAR(255) NOT NULL, times_used INT UNSIGNED NOT NULL);"

fixtures:
	$(EXEC_DB) mysql -uroot -pnotsecret symfony-job-interview -e "insert into uri values (null, 'nicordev.com', 'helloWorld', 0), (null, 'https://www.youtube.com/watch?v=dQw4w9WgXcQ', 'some_token', 0)"

start:
	docker-compose up --detach --remove-orphans --force-recreate

stop:
	docker-compose down --remove-orphans

test:
	$(EXEC_APP) ./vendor/bin/simple-phpunit ${FILTER}

test-case:
	$(EXEC_APP_PHP_BIN_CONSOLE) make:test

composer-install:
	$(EXEC_APP) php -d memory_limit=7G /usr/local/bin/composer install

composer-update:
	$(EXEC_APP) php -d memory_limit=7G /usr/local/bin/composer update

composer-require-dev:
	$(EXEC_APP) php -d memory_limit=7G /usr/local/bin/composer require --dev ${package}

browse:
	@echo https://127.0.0.1/redirect?token=some_token

bash-app:
	$(EXEC_APP) bash

bash-proxy:
	$(EXEC_PROXY) bash

bash-db:
	$(EXEC_DB) bash

pbc:
	$(EXEC_APP_PHP_BIN_CONSOLE) ${cmd}

.PHONY: build start stop install fixtures test composer-update composer-install composer-require-dev database browse bash-app bash-proxy
